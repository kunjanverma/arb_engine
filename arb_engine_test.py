from binance.streams import ThreadedWebsocketManager # Import the Binance Socket Manager
import time
from binance.client import AsyncClient

# Although fine for tutorial purposes, your API Keys should never be placed directly in the script like below.
# You should use a config file (cfg or yaml) to store them and reference when needed.
#PUBLIC =
#SECRET =
PUBLIC = ''
SECRET = ''

# This is our callback function. For now, it just prints messages as they come.
def handle_message(msg):
    print(msg)

# Instantiate a Client
#client = AsyncClient(api_key=PUBLIC, api_secret=SECRET)

# Instantiate a BinanceSocketManager, passing in the client that you instantiated
twm = ThreadedWebsocketManager(PUBLIC, SECRET)
twm.start()

# Start trade socket with 'ETHBTC' and use handle_message to.. handle the message.
#twm.run()
#conn_key = twm.start_trade_socket(symbol='ETHBTC', callback=handle_message)
#twm.start_trade_socket(symbol='ETHBTC', callback=handle_message)
#twm.start_depth_socket(symbol='BTCUSDT', callback=handle_message, depth=5)
#twm.start_ticker_socket(callback=handle_message)
#twm.start_symbol_ticker_socket(symbol='BTCUSDT', callback=handle_message)
#twm.start_symbol_book_ticker_socket(symbol='BTCUSDT', callback=handle_message)
#twm.start_symbol_ticker_futures_socket(symbol='BTCUSDT', callback=handle_message)
#twm.start_futures_multiplex_socket(streams=['btcusdt@bookTicker', 'ethusdt@bookTicker'], callback=handle_message)
# then start the socket manager

# let some data flow..
#time.sleep(10)

# stop the socket manager
twm.join()
#twm.stop_socket(conn_key)
