import threading

class ThreadSafeCounter():
    def __init__(self):
        self.lock = threading.Lock()
        self.counter=0

    def increment(self):
        with self.lock:
            self.counter+=1
            
    def decrement(self):
        with self.lock:
            self.counter-=1
            
'''class OrderSide(enum.Enum):
   BUY = 1
   SELL = 2
   
class OrderType(enum.Enum):
   MARKET = 1
   LIMIT = 2

class TIF(enum.Enum):
    DAY = 0
    GTC = 1#Good Till Cancel
    OPG = 2#At the Opening
    FOK = 3#Fill or Kill
    GTX = 4#Good Till Crossing
    GTD = 5#Good Till date'''