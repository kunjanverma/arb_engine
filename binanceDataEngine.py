import configparser

from configSetting import ConfigSetting
from binance.streams import ThreadedWebsocketManager
from binance.exceptions import BinanceAPIException, BinanceOrderException
from binance.client import Client, AsyncClient
from memory_profiler import profile
#from queue import Queue
from queue import LifoQueue
from threading import Thread
import time
import json
import logging
import asyncio


class BinanceDataEngine:
     
    def __init__(self, config : ConfigSetting, arbEngine):
        self.__config = config
        self.__arbEngine = arbEngine
        self.__client = Client(api_key = config.public_key_BINANCE, api_secret = config.secret_key_BINANCE)
        #self.__client = AsyncClient(api_key = config.public_key_BINANCE, api_secret = config.secret_key_BINANCE)
        self.stream_ticker_list = []
        self.threadedWebsocketManager = ThreadedWebsocketManager(config.public_key_BINANCE , config.secret_key_BINANCE)
        self.sockets=[]
        self.finishTickProcessing=False
        self.arb_exchange = {}
        self.OrderState = {}
        self.assetBalance={}
        self.__spot_queue_dict = {}
        self.spot_tickProcessWorkers = {}
        all_prod = self.__client.get_products()
        all_prod_tickers=[]
        
        for elem in all_prod['data']:
            all_prod_tickers.append(elem['s'])
            
        self.logger = logging.getLogger('Binance')
        self.logger.setLevel(logging.INFO)

        for ticker in config.ticker_list:
            no_dash_ticker = ticker.replace('-', '')
            if no_dash_ticker in all_prod_tickers:
                self.stream_ticker_list.append(no_dash_ticker.lower() + '@bookTicker')
                setattr(self, 'latest_spot_tick_BID_'+no_dash_ticker, None)
                setattr(self, 'latest_spot_tick_ASK_'+no_dash_ticker, None)
                self.__spot_queue_dict[no_dash_ticker] = LifoQueue()
                self.spot_tickProcessWorkers[no_dash_ticker] = Thread(target=self.processSpotTicks, args=(no_dash_ticker,)) 
                
                #Get and store balance of the tickers we are interested in trading
                coin = config.ticker_asset_dict[no_dash_ticker][0]
                ticker_balance = self.__client.get_asset_balance(asset=coin)
                self.assetBalance[coin] = float(ticker_balance['free'])
                ticker_balance = self.__client.get_asset_balance(asset=coin)
                coin = config.ticker_asset_dict[no_dash_ticker][1]
                self.assetBalance[coin] = float(ticker_balance['free'])
                
        self.logger.debug("===========Balances=============")
        for item in self.assetBalance.items():
            self.logger.debug("{0}:{1}".format(item[0],item[1]))
        self.logger.debug("================================")

        '''order = None
        self.logger.info("Sending fil-gbp limit order1")
        try:
            #order = self.__client.order_limit(timeInForce='FOK', symbol='FILUSDT', side='sell', quantity=0.5, price='23.61')
            #order = self.__client.order_market(symbol='FILUSDT', side='sell', quantity=0.5)
            #order=self.__client.cancel_order(orderId=1875686142, symbol='FILUSDT')
        except BinanceAPIException as error:
            self.logger.info("Got exception {0}".format(error.response.text)) 
        else:
            self.logger.info(order)

        self.logger.info("Sending fil-gbp limit order2")
        self.logger.info("Sending fil-gbp limit order3")
        '''

    def adjustAsset(self, ticker, side, fills): 
        asset = self.__config.ticker_asset_dict[ticker]
        for entry in fills:
            volume = float(entry['qty'])
            commission = float(entry['commission'])
            commissionAsset = entry['commissionAsset'].upper()
            price = float(entry['price'])
            if side == 'buy':
                if asset[0] == commissionAsset:
                    self.assetBalance[asset[0]] += (volume - commission)
                else:
                    self.assetBalance[asset[0]] += volume

                if asset[1] == commissionAsset:
                    self.assetBalance[asset[1]] -= (volume*price + commission)
                else:
                    self.assetBalance[asset[1]] -= volume*price
            elif side == 'sell':
                if asset[0] == commissionAsset:
                    self.assetBalance[asset[0]] -= (volume + commission)
                else:
                    self.assetBalance[asset[0]] -= volume

                if asset[1] == commissionAsset:
                    self.assetBalance[asset[1]] += (volume*price - commission)
                else:
                    self.assetBalance[asset[1]] += volume*price
    '''
    {'symbol': 'FILUSDT', 'orderId': 1872889923, 'orderListId': -1, 'clientOrderId': 'RfEga6eWnxLTvdllhrXKoS', 'transactTime': 1644435750540, 'price': '30.00000000', 'origQty': '0.50000000', 'executedQty': '0.00000000', 'cummulativeQuoteQty': '0.00000000', 'status': 'NEW', 'timeInForce': 'GTC', 'type': 'LIMIT', 'side': 'SELL', 'fills': []} 
    {'symbol': 'FILUSDT', 'orderId': 1872871334, 'orderListId': -1, 'clientOrderId': 'iS6LboCoeK1WMYhSX1kvGG', 'transactTime': 1644435117427, 'price': '24.50000000', 'origQty': '0.50000000', 'executedQty': '0.50000000', 'cummulativeQuoteQty': '12.27500000', 'status': 'FILLED', 'timeInForce': 'FOK', 'type': 'LIMIT', 'side': 'SELL', 
     'fills': [{'price': '24.55000000', 'qty': '0.50000000', 'commission': '0.01227500', 'commissionAsset': 'USDT', 'tradeId': 125808494}]}
    {'symbol': 'FILUSDT', 'orderId': 1872876798, 'orderListId': -1, 'clientOrderId': 'bkBIxgKn0I3QMAAciRJctg', 'transactTime': 1644435330055, 'price': '30.00000000', 'origQty': '0.50000000', 'executedQty': '0.00000000', 'cummulativeQuoteQty': '0.00000000', 'status': 'EXPIRED', 'timeInForce': 'FOK', 'type': 'LIMIT', 'side': 'SELL', 'fills': []}
    '''
    async def limitOrder(self, arb_exchange, ticker, price:float, size:float, side:str, TIF:str):
        self.arb_exchange[ticker] = arb_exchange
        self.OrderState[ticker] = ('', 'NEW')
        ret = self.__client.order_limit(timeInForce=TIF, symbol=ticker, side=side, quantity=size, price=str(price))
        if ret['status'] == 'EXPIRED':
            self.OrderState[ticker] = (ret['orderId'], 'REJECTED')
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
            self.__arbEngine.checkAndSendMarketorCancelOrder(arb_exchange, ticker, size, side)
        elif ret['status'] == 'FILLED':
            self.adjustAsset(ret['symbol'], ret['side'], ret['fills'])
            self.OrderState[ticker] = (ret['orderId'], 'FILLED')
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
        
    '''
     {'symbol': 'FILUSDT', 'orderId': 1875935793, 'orderListId': -1, 'clientOrderId': '5oZNIg5baIXOd49dZnJQ8b', 
     'transactTime': 1644524311045, 'price': '0.00000000', 'origQty': '0.50000000', 'executedQty': '0.50000000', 
     'cummulativeQuoteQty': '11.68000000', 'status': 'FILLED', 'timeInForce': 'GTC', 'type': 'MARKET', 'side': 'SELL', 
     'fills': [{'price': '23.36000000', 'qty': '0.50000000', 'commission': '0.01168000', 'commissionAsset': 'USDT', 
     'tradeId': 126004538}]}
    '''
    async def marketOrder(self, ticker, size:float, side:str):
        self.OrderState[ticker] = (ret['orderId'], 'MARKET')
        ret = self.__client.order_market(symbol=ticker, side=side, quantity=size)
        if ret['status'] == 'FILLED':
            self.adjustAsset(ret['symbol'], ret['side'], ret['fills'])
            self.OrderState[ticker] = (ret['orderId'], 'FILLED')
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
   
    '''
     For cancel order, we get an accept or reject as http response. We do not get anything regarding the cancel in
     user data stream. So we need to handle any response from cancel in this method
     {'symbol': 'FILUSDT', 'origClientOrderId': 'alG45R6tEhF8u59v76c4aD', 'orderId': 1875686142, 'orderListId': -1, 
     'clientOrderId': 'ShgmjjfefYrnSXnN1cznTj', 'price': '30.00000000', 'origQty': '0.50000000', 
     'executedQty': '0.00000000', 'cummulativeQuoteQty': '0.00000000', 'status': 'CANCELED', 
     'timeInForce': 'GTC', 'type': 'LIMIT', 'side': 'SELL'} //cancel ack
     {"code":-2011,"msg":"Unknown order sent."} //cancel reject
    ''' 
    async def cancelOrder(self, ticker:str):
        exchOrdId = self.OrderState[ticker][0]
        try:
            ret = self.__client.cancel_order(orderId=int(exchOrdId), symbol=ticker)
        except BinanceAPIException as error: #This case is for cancel reject
            #Ideally We need to send market order. However in binance case this is not required because our 
            #limit order TIF is always FOK and we get the limit order sync respose. So orderstatus can never be 'NEW' 
            self.logger.warning("Got exception for cancel request {0}".format(error.response.text)) 
        else:
            self.OrderState[ticker] = (exchOrdId, 'CANCELLED')
            self.logger.debug("Cancelled Order id:{0} for ticker:{1}".format(exchOrdId, ticker))

        self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()

    '''
     19:24:58,623 Binance ARBITRAGE Engine INFO    User data {'e': 'executionReport', 'E': 1644434698556, 's': 'FILUSDT', 'c': 'web_bd38b2cd8afb40e790341cf5673ffe89', 'S': 'SELL', 'o': 'LIMIT', 'f': 'FOK', 'q': '0.45000000', 'p': '24.48000000', 'P': '0.00000000', 'F': '0.00000000', 'g': -1, 'C': '', 'x': 'NEW', 'X': 'NEW', 'r': 'NONE', 'i': 1872854746, 'l': '0.00000000', 'z': '0.00000000', 'L': '0.00000000', 'n': '0', 'N': None, 'T': 1644434698555, 't': -1, 'I': 3861706924, 'w': True, 'm': False, 'M': False, 'O': 1644434698555, 'Z': '0.00000000', 'Y': '0.00000000', 'Q': '0.00000000'}
     19:24:58,624 Binance ARBITRAGE Engine INFO    User data {'e': 'executionReport', 'E': 1644434698556, 's': 'FILUSDT', 'c': 'web_bd38b2cd8afb40e790341cf5673ffe89', 'S': 'SELL', 'o': 'LIMIT', 'f': 'FOK', 'q': '0.45000000', 'p': '24.48000000', 'P': '0.00000000', 'F': '0.00000000', 'g': -1, 'C': '', 'x': 'TRADE', 'X': 'FILLED', 'r': 'NONE', 'i': 1872854746, 'l': '0.45000000', 'z': '0.45000000', 'L': '24.51000000', 'n': '0.01102950', 'N': 'USDT', 'T': 1644434698555, 't': 125806963, 'I': 3861706925, 'w': False, 'm': False, 'M': True, 'O': 1644434698555, 'Z': '11.02950000', 'Y': '11.02950000', 'Q': '0.00000000'}
     19:24:58,624 Binance ARBITRAGE Engine INFO    User data {'e': 'outboundAccountPosition', 'E': 1644434698556, 'u': 1644434698555, 'B': [{'a': 'BNB', 'f': '0.00000000', 'l': '0.00000000'}, {'a': 'USDT', 'f': '11.15430830', 'l': '0.00000000'}, {'a': 'FIL', 'f': '23.60342000', 'l': '0.00000000'}]}
    '''
    def userDataHandler(self, msg):
        self.logger.info("User data {0}".format(msg))
          
        #This message comes along with executionreport whenever our trade is executed
        if msg['e'] == 'outboundAccountPosition':
            for item in msg['B']:
                self.assetBalance[item['a']] = float(item['f'])
                
    def processSpotTicks(self, ticker):
        q=self.__spot_queue_dict[ticker]
        while not self.finishTickProcessing:
            elem = q.get()
            if not q.empty():
                q.queue.clear()

            spot_bid_prc=float(elem[1])
            spot_bid_vol=float(elem[2])
            spot_ask_prc=float(elem[3])
            spot_ask_vol=float(elem[4])
            last_bid_prc = getattr(self, 'latest_spot_tick_BID_' + ticker)
            last_ask_prc = getattr(self, 'latest_spot_tick_ASK_' + ticker)

            bid_elem = getattr(self.__arbEngine, 'latest_spot_tick_BID_' + ticker).get((last_bid_prc, 'BINANCE'))
            if bid_elem:
                self.logger.trace("Deleting bid element {0}".format(elem))
                del getattr(self.__arbEngine, 'latest_spot_tick_BID_' + ticker)[(last_bid_prc,'BINANCE')]

            ask_elem = getattr(self.__arbEngine, 'latest_spot_tick_ASK_' + ticker).get((last_ask_prc, 'BINANCE'))
            if ask_elem:
                self.logger.trace("Deleting ask element {0}".format(elem))
                del getattr(self.__arbEngine, 'latest_spot_tick_ASK_' + ticker)[(last_ask_prc, 'BINANCE')]

            getattr(self.__arbEngine, 'latest_spot_tick_BID_' + ticker)[(spot_bid_prc, 'BINANCE')] = spot_bid_vol
            getattr(self.__arbEngine, 'latest_spot_tick_ASK_' + ticker)[(spot_ask_prc, 'BINANCE')] = spot_ask_vol
            setattr(self, 'latest_spot_tick_BID_' + ticker, spot_bid_prc)
            setattr(self, 'latest_spot_tick_ASK_' + ticker, spot_ask_prc)
            self.logger.trace("Binance {0}: {1}".format(ticker, elem))
            if self.logger.getEffectiveLevel() == logging.TRACE:
                self.__arbEngine.dumpInfo(self.logger)
            self.__arbEngine.dump()
            self.__arbEngine.checkArbOpportunity(ticker)
        
    def start(self):
        self.threadedWebsocketManager.start()
        for v in self.spot_tickProcessWorkers.values(): #Start all consumers for spot tick data
            v.start()
        #self.spot_userDataWorker.start()
        self.startSpotUserDataSocket()
        self.startSpotTickDataSocket()

    def handleSpotTickData(self, msg):
        self.__spot_queue_dict[msg['data']['s']].put_nowait([time.time(), msg['data']['b'], msg['data']['B'], msg['data']['a'], msg['data']['A']])
        #self.logger.trace("BINANCE: {0}".format(msg))
        
    def startSpotUserDataSocket(self):
        self.sockets.append(self.threadedWebsocketManager.start_user_socket(callback=self.userDataHandler))

    def startSpotTickDataSocket(self):
        self.sockets.append(self.threadedWebsocketManager.start_multiplex_socket(streams=self.stream_ticker_list ,
                                                               callback=self.handleSpotTickData))