import cbpro, time
import logging
import configSetting
from sortedcontainers import sorteddict
import math

class CoinbaseDataEngine(cbpro.WebsocketClient):

    def __init__(self, config : configSetting.ConfigSetting, arbEngine):
        self.__config = config
        self.__arbEngine = arbEngine
        self.public_client = cbpro.PublicClient()
        self.auth_client = cbpro.AuthenticatedClient(key=config.public_key_COINBASE, 
                                                     b64secret = config.secret_key_COINBASE,
                                                     passphrase=config.passphrase_COINBASE)
        self.url = "wss://ws-feed.pro.coinbase.com/"
        self.channels=['level2', 'user']
        self.products= [] 
        self.all_products= [] 
        self.OrderState = {}
        self.assetBalance={}
        self.arb_exchange = {}
        self.open_order_size = {}
        self.taker_fee_rate = {}
        self.logger = logging.getLogger('Coinbase')
        self.logger.setLevel(logging.INFO)
        all_products = self.public_client.get_products()
        all_accounts = self.auth_client.get_accounts()

        for elem in all_products:
            self.all_products.append(elem['id'])

        for ticker in config.ticker_list:
            no_dash_ticker = ticker.replace('-','')
            if (ticker.upper() in self.all_products):
                self.products.append(ticker.upper())
                #Sortedlist to keep top 2 prices on the book
                setattr(self, 'latest_spot_tick_BID_'+no_dash_ticker, sorteddict.SortedDict()) 
                setattr(self, 'latest_spot_tick_ASK_'+no_dash_ticker, sorteddict.SortedDict())

                #Get balance of the tickers we are interested in trading
                for account in all_accounts:
                    if account['currency'] in config.ticker_asset_dict[no_dash_ticker]:
                        self.assetBalance[account['currency'].upper()] = float(account['balance'])

        super(CoinbaseDataEngine, self).__init__(products = self.products,
                                                 auth = True,
                                                 api_key = config.public_key_COINBASE, 
                                                 api_secret = config.secret_key_COINBASE,
                                                 api_passphrase = config.passphrase_COINBASE,
                                                 channels = self.channels)
                            
        self.logger.debug("===========Balances=============")
        for item in self.assetBalance.items():
            self.logger.debug("{0}:{1}".format(item[0],item[1]))
        self.logger.debug("===============================")

    '''{'message': 'time in force'} //FOK rejected
    
       {'id': '462a9931-ee5c-4a77-bd12-3afd8ba3920b', 'price': '17.85', 'size': '0.5', 'product_id': 'FIL-GBP', 
       'side': 'sell', 'stp': 'dc', 'type': 'limit', 'time_in_force': 'FOK', 'post_only': False, 
       'created_at': '2022-02-07T19:49:43.240676Z', 'fill_fees': '0', 'filled_size': '0', 'executed_value': '0', 
       'status': 'pending', 'settled': False} //FOK pending
       
       {'id': '3326bfd2-9034-432b-b635-56d680bb2d39', 'price': '25', 'size': '0.5', 'product_id': 'FIL-GBP', 
       'side': 'sell', 'stp': 'dc', 'type': 'limit', 'time_in_force': 'GTC', 'post_only': False, 
       'created_at': '2022-02-10T12:12:09.365358Z', 'fill_fees': '0', 'filled_size': '0', 'executed_value': '0', 
       'status': 'pending', 'settled': False} /GTC pending
    '''
    async def limitOrder(self, arb_exchange, ticker, price:float, size:float, side:str, TIF:str):
        cb_ticker = self.__config.ticker_asset_dict[ticker][2]
        self.OrderState[cb_ticker] = ('', 'NEW')
        ret = self.auth_client.place_limit_order(cb_ticker, side, price, size, TIF)
        #We got a reject to the FOK limit order we tried to send. We need to send market order for other exchange to nullify the counter FOK we sent
        #In this case, we do not get any execution report, so we need to handle it here itself
        if 'message' in ret:
            self.logger.debug("Limit order for {0} at price:{1}, size: {2} rejected".format(ticker, price, size))
            self.OrderState[cb_ticker] = (ret['id'], 'REJECTED')
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
            self.__arbEngine.checkAndSendMarketorCancelOrder(arb_exchange, ticker, size, side)
            
    '''{'id': '9d4752b5-4728-4ccb-9696-394a3d427485', 'size': '0.5', 'product_id': 'FIL-GBP', 'side': 'sell', 
    'stp': 'dc', 'type': 'market', 'post_only': False, 'created_at': '2022-02-10T14:08:07.425599Z', 'fill_fees': '0', 
    'filled_size': '0', 'executed_value': '0', 'status': 'pending', 'settled': False}
    '''
    async def marketOrder(self, ticker, size:float, side:str):
        cb_ticker = self.__config.ticker_asset_dict[ticker][2]
        exchOrdId = self.OrderState[ticker][0]
        self.OrderState[ticker] = (exchOrdId, 'MARKET')
        ret = self.auth_client.place_market_order(cb_ticker, side, size)
        if 'message' in ret:
            self.OrderState[ticker] = (exchOrdId, 'REJECTED')
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
            #TODO We need to raise an alert here
            self.logger.warning("Market {0} order for ticker:{0}, size:{3} and got rejected".format(side, cb_ticker,size ))

    '''{'message': 'order not found'} //cancel reject. cancel reject needs to be handled here since we wont get any executionReport
    7168071d-3f5d-4c97-a8bf-6c2a57ccea73 //successful cancel. We will handle sucessful cancel in 'done' executionReport
    '''
    async def cancelOrder(self, ticker):
        exchOrdId = self.OrderState[ticker][0]
        cb_ticker = self.__config.ticker_asset_dict[ticker][2]
        ret = self.auth_client.cancel_order(exchOrdId)

        #cancel order has been rejected for whatever reason, no such order present or already cancelled
        #In this case, we do not get any execution report, so we need to handle it here itself. This can 
        #happen if by the time our cancel order reaches the exchange, our order gets filled by the exchange.
        #TODO We should try to check order state and send market/cancel order again.
        if 'message' in ret:
            #ret = self.auth_client.get_order()
            #self.OrderState[(cb_ticker, exchOrdId)] = ret['status'].upper()
            self.logger.warning("Tried to cancel order with exchOrdId {0} and got cancel reject".format(exchOrdId))
            self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()

    def on_open(self):
        pass

    def on_message(self, msg):
        self.logger.trace(msg)
        '''if msg['type'] == 'subscriptions':
            self.logger.info("Sending fil-gbp limit order")
            #ret = self.auth_client.place_limit_order('FIL-GBP', side='sell', price=17.35, size=0.5, time_in_force='FOK')
            #ret = self.auth_client.place_market_order('FIL-GBP', side='sell', size=0.5)
            #ret = self.auth_client.cancel_order('2c7f0b48-bb4c-4d22-9042-3e9962cf6cbd')
            self.logger.info("Sending fil-gbp limit order2")
            self.logger.info(ret)
            self.logger.info("Sending fil-gbp limit order3")'''
            
        if msg['type'] == 'snapshot':
            no_dash_ticker = msg['product_id'].replace('-', '') 
            #self.logger.info(msg)
            
            bid_dict = getattr(self, 'latest_spot_tick_BID_' + no_dash_ticker)
            ask_dict = getattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker)
            for item in msg['asks']:
                ask_dict[float(item[0])] = float(item[1])
            for item in msg['bids']:
                bid_dict[float(item[0])] = float(item[1])
                
            spot_bid_prc = bid_dict.peekitem()[0]
            spot_bid_vol = bid_dict.peekitem()[1]
            spot_ask_prc = ask_dict.peekitem(0)[0]
            spot_ask_vol = ask_dict.peekitem(0)[1]
            getattr(self.__arbEngine, 'latest_spot_tick_BID_' + no_dash_ticker)[(spot_bid_prc, 'COINBASE')] = spot_bid_vol
            getattr(self.__arbEngine, 'latest_spot_tick_ASK_' + no_dash_ticker)[(spot_ask_prc, 'COINBASE')] = spot_ask_vol
            self.logger.trace("Snapshot {0} ask_prc:{1} ask_vol:{2} bid_prc:{3} bid_vol:{4}".format(no_dash_ticker, 
                        spot_ask_prc, spot_ask_vol, spot_bid_prc, spot_bid_vol))

            if self.logger.getEffectiveLevel() == logging.TRACE:
                self.__arbEngine.dumpInfo(self.logger)
            self.__arbEngine.dump()

        elif msg['type'] == 'l2update':
            no_dash_ticker = msg['product_id'].replace('-', '') 
            changes = msg['changes']
            #self.logger.trace(msg)
                        
            for elem in changes:
                best_bid_ask_changed = False
                if elem[0] == 'buy':
                    bid_orderbook_dict = getattr(self, 'latest_spot_tick_BID_' + no_dash_ticker)
                    best_orderbook_bid_prc = bid_orderbook_dict.peekitem()[0]
                    spot_bid_prc = float(elem[1])
                    spot_bid_vol = float(elem[2])
                    arbEngine_bid_dict = getattr(self.__arbEngine, 'latest_spot_tick_BID_' + no_dash_ticker)
                    # A volume of 0 means we need to delete that entry from orderbook
                    if spot_bid_vol == 0:
                        del bid_orderbook_dict[spot_bid_prc]
                        if math.isclose(spot_bid_prc, best_orderbook_bid_prc):
                            best_bid_ask_changed = True
                            arbEngine_bid_dict_elem = next(filter(lambda elem: elem[0][1] == 'COINBASE', arbEngine_bid_dict.items()), None)
                            del arbEngine_bid_dict[arbEngine_bid_dict_elem[0]]
                            new_best_orderbook_bid = bid_orderbook_dict.peekitem()[0]
                            new_best_orderbook_vol = bid_orderbook_dict.peekitem()[1]
                            arbEngine_bid_dict[(new_best_orderbook_bid, 'COINBASE')] = new_best_orderbook_vol
                            self.logger.debug('New best_bid after deleting old best_bid {0}: {1},{2}'.format(no_dash_ticker, new_best_orderbook_bid, new_best_orderbook_vol))
                            if self.logger.getEffectiveLevel() == logging.TRACE:
                                self.__arbEngine.dumpInfo(self.logger)
                            self.__arbEngine.dump()
                            continue
                    else:    
                        bid_orderbook_dict[spot_bid_prc] = spot_bid_vol #update coinbase internal orderbook
                    
                    #In the condition below, this tick is not the top of the book tick or its an update
                    if (spot_bid_prc < best_orderbook_bid_prc):
                        continue
                    else: #This is top of book update, update the arbEngine dict
                        best_bid_ask_changed = True
                        arbEngine_bid_dict_elem = next(filter(lambda elem: elem[0][1] == 'COINBASE', arbEngine_bid_dict.items()), None)
                        if arbEngine_bid_dict_elem is not None: 
                            del arbEngine_bid_dict[arbEngine_bid_dict_elem[0]]
                        arbEngine_bid_dict[(spot_bid_prc, 'COINBASE')] = spot_bid_vol
                        self.logger.debug('Better best bid {0}: {1},{2}'.format(no_dash_ticker, spot_bid_prc, spot_bid_vol))
                        
                if elem[0] == 'sell':
                    ask_orderbook_dict = getattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker)
                    best_orderbook_ask_prc = ask_orderbook_dict.peekitem(0)[0]
                    spot_ask_prc = float(elem[1])
                    spot_ask_vol = float(elem[2])
                    arbEngine_ask_dict = getattr(self.__arbEngine, 'latest_spot_tick_ASK_' + no_dash_ticker)
                    # A volume of 0 means we need to delete that entry from orderbook
                    if spot_ask_vol == 0:
                        del ask_orderbook_dict[spot_ask_prc]
                        if math.isclose(spot_ask_prc, best_orderbook_ask_prc):
                            best_bid_ask_changed = True
                            arbEngine_ask_dict_elem = next(filter(lambda elem: elem[0][1] == 'COINBASE', arbEngine_ask_dict.items()), None)
                            if arbEngine_ask_dict_elem is not None: 
                                del arbEngine_ask_dict[arbEngine_ask_dict_elem[0]]
                            new_best_orderbook_ask_prc = ask_orderbook_dict.peekitem(0)[0]
                            new_best_orderbook_ask_vol = ask_orderbook_dict.peekitem(0)[1]
                            arbEngine_ask_dict[(new_best_orderbook_ask_prc, 'COINBASE')] = new_best_orderbook_ask_vol
                            self.logger.debug('New best_ask after deleting old best_ask {0}: {1},{2}'.format(no_dash_ticker, new_best_orderbook_ask_prc, new_best_orderbook_ask_vol))
                            if self.logger.getEffectiveLevel() == logging.TRACE:
                                self.__arbEngine.dumpInfo(self.logger)
                            self.__arbEngine.dump()
                            continue
                    else:    
                        ask_orderbook_dict[spot_ask_prc] = spot_ask_vol #update our internal orderbook
                    
                    #In the condition below, this tick is not the top of the book tick
                    if (spot_ask_prc > best_orderbook_ask_prc):
                        continue
                    else: #This is top of book update, update the arbEngine dict
                        best_bid_ask_changed = True
                        arbEngine_ask_dict_elem = next(filter(lambda elem: elem[0][1] == 'COINBASE', arbEngine_ask_dict.items()), None)
                        if arbEngine_ask_dict_elem is not None: 
                            del arbEngine_ask_dict[arbEngine_ask_dict_elem[0]]
                        arbEngine_ask_dict[(spot_ask_prc, 'COINBASE')] = spot_ask_vol
                        self.logger.debug('Better best ask {0}: {1},{2}'.format(no_dash_ticker, spot_ask_prc, spot_ask_vol))

                self.logger.trace('{0}: {1}'.format(no_dash_ticker, elem))
                if best_bid_ask_changed:
                    if self.logger.getEffectiveLevel() == logging.TRACE:
                        self.__arbEngine.dumpInfo(self.logger)
                    self.__arbEngine.dump()
                    self.__arbEngine.checkArbOpportunity(no_dash_ticker)
                    
            '''{'trade_id': 954092, 'maker_order_id': 'fdc62dde-d03f-48c4-b839-050811bf5286', 
            'taker_order_id': '5129722b-b201-4717-adb9-d20ab8cd16ef', 'size': '0.5', 'price': '17.38', 
            'taker_profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 'taker_user_id': '5b204f2db004a510ddaf7718', 
            'taker_fee_rate': '0.005', 'type': 'match', 'side': 'buy', 'product_id': 'FIL-GBP', 
            'time': '2022-02-10T13:39:07.804140Z', 'sequence': 580450880, 'profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 
            'user_id': '5b204f2db004a510ddaf7718'}

             {'trade_id': 954303, 'maker_order_id': '884c18e7-4091-4eea-a75e-6d7731269adf', 
             'taker_order_id': '9d4752b5-4728-4ccb-9696-394a3d427485', 'size': '0.5', 'price': '16.95', 
             'taker_profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 'taker_user_id': '5b204f2db004a510ddaf7718', 
             'taker_fee_rate': '0.005', 'type': 'match', 'side': 'buy', 'product_id': 'FIL-GBP', 
             'time': '2022-02-10T14:08:07.433792Z', 'sequence': 580508774, 
             'profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 'user_id': '5b204f2db004a510ddaf7718'}
            '''
        elif msg['type'] == 'match':
            self.logger.info(msg)
            ticker = msg['product_id'].replace('-', '')
            #self.taker_fee_rate[ticker] = float(msg['taker_fee_rate'])
            #self.open_order_size[ticker] = float(msg['size'])
            taker_fee_rate = float(msg['taker_fee_rate'])
            assets = msg['product_id'].split('-')
            side = msg['side']
            volume =  float(msg['size'])
            price = float(msg['price'])
            if side == 'buy':
                        self.assetBalance[assets[0]] += volume*(1 - taker_fee_rate)
                        self.assetBalance[assets[1]] -= volume*price
            elif side == 'sell':
                        self.assetBalance[assets[0]] -= volume
                        self.assetBalance[assets[1]] += volume*price*(1 - taker_fee_rate)
            #TODO Send broadcast msg to all other runnig instances for change in volumes of these assets
            
            self.logger.info("{0}_Balance:{1}, {2}_Balance:{3}".format(assets[0], self.assetBalance[assets[0]],
                                                                        assets[1], self.assetBalance[assets[1]]))
            
        elif msg['type'] == 'done':
            self.logger.info(msg)
            ticker = msg['product_id'].replace('-', '')
            exchOrdId = msg['order_id']

            '''{'order_id': '5129722b-b201-4717-adb9-d20ab8cd16ef', 'reason': 'filled', 'price': '17.35', 
            'remaining_size': '0', 'type': 'done', 'side': 'sell', 'product_id': 'FIL-GBP', 
            'time': '2022-02-10T13:39:07.804140Z', 'sequence': 580450881, 'profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 
            'user_id': '5b204f2db004a510ddaf7718'} //Limit order fill, no size
             {'order_id': '9d4752b5-4728-4ccb-9696-394a3d427485', 'reason': 'filled', 'remaining_size': '0', 
              'type': 'done', 'side': 'sell', 'product_id': 'FIL-GBP', 'time': '2022-02-10T14:08:07.433792Z', 
              'sequence': 580508775, 'profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 
              'user_id': '5b204f2db004a510ddaf7718'} //Market order fill. It has no price or size
              '''
            if msg['reason'] == 'filled':
                self.OrderState[ticker] = (exchOrdId, 'FILLED')
                self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()

            '''{'order_id': '2c7f0b48-bb4c-4d22-9042-3e9962cf6cbd', 'reason': 'canceled', 'price': '25', 
            'remaining_size': '0.5', 'type': 'done', 'side': 'sell', 'product_id': 'FIL-GBP', 
            'time': '2022-02-10T13:08:11.422646Z', 'sequence': 580408561, 'profile_id': '33b916fe-50e5-416f-8076-f15d2faa7ca7', 
            'user_id': '5b204f2db004a510ddaf7718'}'''

            if msg['reason'] == 'canceled':
                self.OrderState[ticker] = (exchOrdId, 'CANCELLED')
                self.logger.debug("Cancelled Order id:{0} for ticker:{1}".format(exchOrdId, ticker))
                self.__arbEngine.ticker_outstanding_order_dict[ticker].decrement()
                
    def on_close(self):
        print("-- Goodbye! --")