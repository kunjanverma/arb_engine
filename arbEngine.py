from asyncio.log import logger
import queue
from configSetting import ConfigSetting
from binanceDataEngine import BinanceDataEngine
from coinbaseDataEngine import CoinbaseDataEngine
from threading import Thread, Lock
from sortedcontainers import sorteddict
import logging
import logging.handlers
import time
import common
import os
import asyncio
import threading

class ArbEngine:
    
    def __init__(self, config):
        self.__tick_engine = {}
        self.__config = config
        self.threadDict={}
        self.exchanges = config.exchanges
        self.ticker_outstanding_order_dict = {}
        if 'BINANCE' in self.exchanges:
            self.__tick_engine['BINANCE']=BinanceDataEngine(config, self)
        if 'COINBASE' in self.exchanges:
            self.__tick_engine['COINBASE']=CoinbaseDataEngine(config, self)
        #if 'KUCOIN' in self.exchanges:
        #    self.__tick_engine['KUCOIN']=KucoinDataEngine(config)
        
        for ticker in config.ticker_list:
            no_dash_ticker = ticker.replace('-', '')
            #setattr(self, 'latest_spot_tick_BID_' + no_dash_ticker, sorteddict.SortedDict(key = lambda elem:elem[0][0]))
            #setattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker, sorteddict.SortedDict(key = lambda elem:elem[0][0]))
            setattr(self, 'latest_spot_tick_BID_' + no_dash_ticker, sorteddict.SortedDict())
            setattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker, sorteddict.SortedDict())
            self.ticker_outstanding_order_dict[no_dash_ticker] = common.ThreadSafeCounter()
            
        self.logger = logging.getLogger('ArbEngine')
        self.logger.setLevel(logging.DEBUG)

        self.mutex = Lock()
         
    def dumpInfo(self, logger):
        self.mutex.acquire()
        for ticker in self.__config.ticker_list:
            no_dash_ticker = ticker.replace('-', '')
            bid_dict = getattr(self, 'latest_spot_tick_BID_' + no_dash_ticker)
            ask_dict = getattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker)
            logger.trace("===========Ticks===============")
            for item in reversed(bid_dict.items()):
                logger.trace("Bid {0}:: {1}: [{2},{3}]".format(no_dash_ticker,item[0][0], item[0][1], item[1] ))
            for item in ask_dict.items():
                logger.trace("Ask {0}:: {1}: [{2},{3}]".format(no_dash_ticker, item[0][0], item[0][1], item[1] ))
            logger.trace("===============================")

        logger.trace("===========Wallets==============")
        for exchange in self.exchanges:
            logger.trace("==========={0}=============".format(exchange))
            for item in self.__tick_engine[exchange].assetBalance.items():
                logger.trace("{0}:{1}".format(item[0],item[1]))
            logger.trace("===============================")
            
        self.mutex.release()

    def dump(self):
        os.system('clear')
        for ticker in self.__config.ticker_list:
            no_dash_ticker = ticker.replace('-', '')
            bid_dict = getattr(self, 'latest_spot_tick_BID_' + no_dash_ticker)
            ask_dict = getattr(self, 'latest_spot_tick_ASK_' + no_dash_ticker)
            print("===============================")
            print("===========Bids===============")
            for item in reversed(bid_dict.items()):
                print("Bid {0}:: {1}: [{2},{3}]".format(no_dash_ticker,item[0][0], item[0][1], item[1] ))
            print("===========Asks===============")
            for item in ask_dict.items():
                print("Ask {0}:: {1}: [{2},{3}]".format(no_dash_ticker, item[0][0], item[0][1], item[1] ))
            print("===============================")

        print("===========Wallets==============")
        for exchange in self.exchanges:
            print("==========={0}=============".format(exchange))
            for item in self.__tick_engine[exchange].assetBalance.items():
                print("{0}:{1}".format(item[0],item[1]))
            print("===============================")

    def start(self):
        for v in  self.__tick_engine.values():
            v.start()

    def sendArbOrders(self,ask_item, bid_item, ticker, arb_vol):
            #send buy and sell order here
            order_loop = asyncio.get_event_loop()
            send_order_tasks = []
            send_order_tasks.append(order_loop.create_task(self.limitOrder(ask_item[0][1], bid_item[0][1], ticker, ask_item[0][0], arb_vol, 'buy')))
            send_order_tasks.append(order_loop.create_task(self.limitOrder(bid_item[0][1], ask_item[0][1], ticker, bid_item[0][0], arb_vol, 'sell')))
            order_loop.run_until_complete(asyncio.gather(*send_order_tasks))
            self.ticker_outstanding_order_dict[ticker].counter = 2 #Since there are now 2 outstanding orders on this ticker
            th_id = threading.current_thread().ident
            self.threadDict.pop(th_id)

    async def limitOrder(self, exchange, arb_exchange, ticker, price:float, size:float, side:str, TIF:str='FOK'):
        #TODO data engine implementation of async limitorder should populate self.arb_exchange[ticker]
        self.ticker_outstanding_order_dict[ticker].increment()
        self.__tick_engine[exchange].limitOrder(arb_exchange, ticker, price, size, side, TIF)

    async def marketOrder(self, exchange, ticker, size:float, side:str):
        self.ticker_outstanding_order_dict[ticker].increment()
        self.__tick_engine[exchange].marketOrder(ticker, size, side)

    async def cancelOrder(self, exchange, ticker):
        self.ticker_outstanding_order_dict[ticker].increment()
        self.__tick_engine[exchange].cancelOrder(ticker)
   
    def checkAndSendMarketorCancelOrder(self, exchange, ticker, size:float, side:str):
        #Check here first if the order sent by checkArbOpportunity() to the exchange has already been cancelled(it was FOK).
        #If so, no need to send it
        exch = self.__tick_engine[exchange]
        if exch.OrderState[ticker][1] == 'NEW':
            self.cancelOrder(ticker)
        elif exch.OrderState[ticker][1] == 'FILLED':
            self.marketOrder(exchange, ticker, size, side)
        
    def getWalletBalance(self, exchange, asset):
        #self.logger.debug(self.__tick_engine[exchange])
        return self.__tick_engine[exchange].assetBalance[asset]
            
    #TODO We cannot call this directly from tick data handlers. We need a task queue and we push the task(below method) in the 
    #task queue
    def checkArbOpportunity(self, ticker):
        #if self.ticker_outstanding_order_dict[ticker].counter != 0:
        #    return
        
        bid_dict = getattr(self, 'latest_spot_tick_BID_' + ticker)
        ask_dict = getattr(self, 'latest_spot_tick_ASK_' + ticker)
        for ask_item in ask_dict.items():
            for bid_item in reversed(bid_dict.items()):
                if  100 * (bid_item[0][0] - ask_item[0][0])/(bid_item[0][0] + ask_item[0][0]) > self.__config.arb_percent:
                    buy_vol = ask_item[1]
                    sell_vol = bid_item[1]
                    arb_vol = min(buy_vol, sell_vol)

                    if arb_vol > 0:#Also arb_vol should be greater than respective balances on exchanges 
                        ticker_entry = self.__config.ticker_asset_dict[ticker]
                        balance1 = self.getWalletBalance(exchange = ask_item[0][1], asset = ticker_entry[1])
                        balance2 = self.getWalletBalance(exchange = bid_item[0][1], asset = ticker_entry[0])
                       
                        #TODO correct check is that arb vol is less than 1% of day start portfolio 
                        if (balance1 <= arb_vol*ask_item[0][0]*1.04) or (balance2 <= arb_vol):
                            self.logger.info("Cannot arbitrage {0} at volume {1} on exchanges {2} and {3}, not enough balance"
                                             .format(ticker, arb_vol, ask_item[0][1], bid_item[0][1]))
                            self.logger.info("{0} buy: Price={1}, Vol={2}".format(ask_item[0][1], ask_item[0][0], ask_item[1]))
                            self.logger.info("{0} sell: Price={1}, Vol={2}".format(bid_item[0][1], bid_item[0][0], bid_item[1]))
                            #TODO transfer balance between accounts
                            return
                        #th = Thread(target=self.sendArbOrders, args=(ask_item, bid_item, ticker, arb_vol))
                        #self.threadDict[th.ident]=th
                        #th.start()
                        
                        self.logger.info("Sending {0} buy order to {1} at limit price {2}, volume {3}".format(ticker, ask_item[1],
                                            ask_item[0][0], arb_vol))
                        self.logger.info("Sending {0} sell order to {1} at limit price {2}, volume {3}".format(ticker, bid_item[1],
                                            bid_item[0][0], arb_vol))
                return

    def end(self):
        for th in self.threadDict.values():
            th.join()
        if 'BINANCE' in self.exchanges:
            self.__tick_engine['BINANCE'].finishTickProcessing=True;
            for t in self.__tick_engine['BINANCE'].spot_tickProcessWorkers.values():
                t.join()
            self.__tick_engine['BINANCE'].threadedWebsocketManager.stop()
            self.__tick_engine['BINANCE'].threadedWebsocketManager.join()
        if 'COINBASE' in self.exchanges:
            self.__tick_engine['COINBASE'].close()
            