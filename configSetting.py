
from stat import FILE_ATTRIBUTE_NO_SCRUB_DATA


class ConfigSetting:
  def __init__(self, config):
    self.__config = config
    self.ticker_file = None
    for section in config.sections():
      if section == 'GLOBAL':
        #self.exchange_str = config[section]['EXCHANGE']
        self.exchanges = config[section]['EXCHANGE'].split(",")
        self.arb_percent = float(config[section]['ARB_PERCENTAGE'])
        self.ticker_file = config[section]['TICKER_FILE']
        break

    for section in config.sections():
      if section in self.exchanges:
        setattr(self, 'public_key_'+section, config[section]['PUBLIC'])
        setattr(self, 'secret_key_'+section, config[section]['SECRET'])
        setattr(self, 'passphrase_'+section, config[section]['PASSPHRASE'])

    ticker_list = []
    self.ticker_asset_dict={}
    with open(self.ticker_file) as f:
      ticker_list = f.readlines()
      ticker_list = [ticker.upper() for ticker in ticker_list if ticker[0] !='#' ]
    self.ticker_list = [x.strip() for x in ticker_list]
    for ticker in self.ticker_list:
      self.ticker_asset_dict[ticker.replace('-', '')] = tuple(ticker.split('-')) + (ticker,)
    #for entry in self.ticker_asset_dict.items():
    #  print (entry)