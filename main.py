from configparser import ConfigParser
from configSetting import ConfigSetting
from arbEngine import ArbEngine
import logging

def trace(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(logging.TRACE):
        self._log(logging.TRACE, message, args, **kws)
    
def main():
    logging.basicConfig(filename='log/app.log',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(filename)s:%(funcName)s:%(lineno)d %(levelname)s   %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.ERROR)
    logging.TRACE = 9 
    logging.addLevelName(logging.TRACE, "TRACE")

    logging.Logger.trace = trace
    logger=logging.getLogger('ARBITRAGE Engine Application')
    logger.setLevel(logging.DEBUG)
    
    configparser = ConfigParser()
    configparser.read("config/setting.cfg")
    config=ConfigSetting(configparser)
    
    logger.info("======== Loaded Setting Config File ==============")
    arbEngine = ArbEngine(config)
    arbEngine.start()
    input("Press Enter to continue...")
    arbEngine.end()

if __name__ == "__main__":
    main()